---
layout: page
title: "Job Opportunity - Director, Business Planning"
---

## Update: June 2018

**This position has been filled. Thank you for your interest!**

## Overview

### DIAL Overview

DIAL (Digital Impact Alliance) originated to bring the public and private sectors together to realize an inclusive digital society that connects everyone to life-enhancing and life-enabling technology. DIAL is staffed by a global team of technology researchers, developers, investors, negotiators, and policymakers. It is supported by world-class foundations and development agencies and guided by a board of leading emerging market entrepreneurs, technologists and international development experts. With this leadership, DIAL is uniquely positioned to serve as a neutral broker, bringing together governments, industries, and other stakeholders in international development to promote new solutions to old problems.  

DIAL is investing in three big bets. One of these, DIAL’s Open Source Center (OSC), provides a collaborative space and services to open source projects used in global development. The OSC assists proven technology platforms to mature by working with open source projects to establish effective governance, deepen software project management, improve technical architecture, build effective contributor and implementer communities, and develop sustainable project financing models. More information about the program is available online: <http://osc.dial.community>

### Position Overview

The OSC is expanding its team to bring on board more strategic planning, program management, and business expertise. These skills will be applied to the challenge of scaling the OSC’s services and securing its ongoing operations, as well as providing guidance to OSC client projects.  

The Director of Business Planning will be responsible for designing the OSC’s strategic plan for financial sustainability, setting growth targets, and working with the OSC’s Director of Community and Director of Technology to achieve these targets.  

The Director of Business Planning will develop a financial model for expansion of the OSC’s services and the ongoing operations of the OSC beyond its existing multi-year runway of core funding. S/he will analyze analogous revenue models, work with the other directors to determine service delivery costs and capacity limitations, design sustainable financial models, and test key components of the model. S/he will work with colleagues to design and implement new services, refine existing services and operating models as needed.  

The director must be able to develop parallel roadmaps for implementing and proving out various aspects of the overall financial model. The director must have excellent leadership, collaboration, communication, presentation, and interpersonal skills.

## Responsibilities

- Design the long-term program growth and sustainability plan for the OSC. Identify sources of revenue and funding for the OSC’s core staff and programmatic activities. Research, analyze, and develop cost and capacity models for service delivery.
- Lead the implementation of the revenue generating components of the plan (e.g. establish a fee-for-services structure and underlying pricing model)
- Lead the development and implementation of the operational, measurement, and cost management components of the plan (e.g. establish the operating and cost models for a fee-for-services service delivery model)
- Provide guidance to client projects who are developing their own funding and sustainability plans, as time and capacity allows

## Responsibilities Shared with Other Directors

- In close collaboration with the OSC's Director of Community and Director of Technology, create a holistic approach for the program. Deliver revisions and adaptations to the governance and strategy to meet the changing needs of OSC clients
- Consult with other OSC staff and key stakeholders as part of the ongoing design, implementation, and improvement of OSC governance structures and processes  
- Serve as a “public face” to the OSC. Act as a representative of the project to partners and at speaking at events. Raise awareness of the OSC and its offerings
- Advocate for collaboration among T4D ecosystem actors including open source projects, funders, and technology implementers
- Advocate for the adoption of the Principles of Digital Development (PDD)

## Selection Criteria

- 5+ years of experience in product management or similar role developing and implementing software or software-services pricing models at a technology or technology services company.  
- Business model development and financial modelling experience. Exposure to business modelling in commercial and not-for-profit sectors preferred
- Experience with open source product/software-service models, open source software development process, and open source community dynamics preferred
- 5+ years of experience in international development, humanitarian relief, ICT4D, Tech4Good, or related sectors highly preferred
- A working understanding of software IP and licensing, including open source-related considerations
- Excellent relationship building skills, with demonstrated ability to work with partners and colleagues at all levels
- Ability to collaborate across a matrixed, cross-functional organization to elicit buy-in for the product vision and consensus on priorities
- A flexible skillset and resourceful approach and ability to be extremely effective working independently
- A proactive approach to seeking opportunities and proposing solutions
- Servant-leadership and ability to take ownership of the product area assigned
- Experience performing under pressure and prioritizing work to achieve deadlines
- Exceptional written, verbal, and interpersonal communication skills appropriate for diverse internal and external audiences
- Outstanding planning, time management, and organizational skills
- Excellent relationship building skills, with demonstrated ability to work with partners and colleagues at all levels
- Ability to collaborate across a matrixed, cross-functional organization to elicit buy-in for the product vision and consensus on priorities
- Bachelor’s degree or equivalent in business and/or computer science related fields. Advanced degree in computer science, business administration (MBA), product marketing management, sustainability management, or new product and innovation management preferred
- Willingness and enthusiasm for travel to Least Developed Countries (LDCs) up to 30% of the time

This is a full-time direct-hire opportunity to work at the cutting edge of technology and international development. DIAL is based in Washington, DC, and colleagues for this position are located in multiple cities around the world. Applicants should demonstrate a willingness to commit to, and enthusiasm for traveling to Least Developed Countries (LDCs) up to 30% of the time.

The team will consider applicants living outside of the DC area who are interested in relocating to join the team, as well as individuals interested in working remotely from high-tech hubs such as Seattle, San Francisco, New York City, or Portland. Non-US locations will be considered.

## Benefits & Compensation

For full-time employees we offer an excellent range of benefits, including a choice of two health plans through CareFirst (PPO or HDHP), dental insurance, vision discount plan, Life Insurance, Supplemental Life Insurance, Short-Term Disability, Long-Term Disability, Long-Term Care Insurance, Health Club Discounts, Commuter Benefits, and Employee Assistance Program.


12 Paid holidays, 4 weeks of vacation, 10 sick days, 3 personal days and 8 weeks of parental leave (mother or father), access to the UNFCU Credit Union, 150% match on our 403b retirement plan, flexible spending accounts, health savings accounts, and discounted Zipcar membership.

## Apply

Please visit <https://careers-unfoundation.icims.com/jobs/1568/director,-business-planning-dial-opensource-service-center/job?mobile=false&width=965&height=500&bga=true&needsRedirect=false&jan1offset=-300&jun1offset=-240> to apply.
