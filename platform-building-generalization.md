---
layout: page
title: "OSC Strategic Grant Program - Platform Building and Generalization"
---

## Annex 3 - Platform Building and Generalization

### Objective

DIAL has identified a critical challenge for free & open source software (FOSS) projects serving the sector: software product development is typically tied to programmatic goals, which results in tools that are specialized to a particular sector or use case. These products have limited reach and are therefore more challenging to maintain (due to lower demand) and cannot achieve the broad impact that a more general platform could in multiple sectors.

### Scope of Work

This grant aims to provide a path for refactoring to reduce technical debt and increase modularity, abstraction and extensibility (“platformification”) to products in the FOSS ecosystem in order to broaden their reach. Applicants should be project decision-makers who can build a roadmap involving documentation, feature specifications, task breakdown, and final execution of implementation.

Example projects that would be within scope for this grant might include:

* Refactoring a sector-specific product into a modular or microservice architecture, separating out platform-level components into a separate product/repository, and writing developer documentation for creating additional modules for other sectors.
* Providing user-configurable localization/internationalization capabilities for an existing open source product.
* Adding multi-tenancy support for an existing open source product.

### Additional Eligibility Criteria

* Target product(s) must have multiple stakeholders: contributors to--and users of—the product represent and/or are funded by multiple organizations.
* Upon completion of the proposed activities, target product(s) must provide useful functionality to multiple sectors or domains in the development/humanitarian space (Health and Education, e.g.).

[Back to Strategic Grant Program Overview](stratagrant.html)
