---
layout: page
title: "OSC Strategic Grant Program - Enterprise-Level Quality Improvements"
---

## Annex 1 - Enterprise-Level Quality Improvements

### Objective

DIAL has identified a critical challenge for free & open source software (FOSS) projects serving the sector: software product development is typically tied to programmatic goals, which can result in products that are unable to achieve enterprise-level security, stability and quality during the lifecycle of the programmatic funding, and who struggle to find resources for long-term sustainability and maintenance of those products. Additionally, as new products gain popularity, older projects need to be able build additional interoperability features to continue to maximize their impact and reach.

Example projects that would be within scope for this grant might include:
* Packaging and simple install capabilities
* Containerization
* Adding CI/CD and increased automated test coverage
* Refactoring the product architecture to accommodate higher scale
* Increased offline/low connectivity support
* Developer integration APIs

### Scope of Work

This grant aims to provide a path for enhancing the maturity, ease of deployment, scalability, and interoperability of FOSS projects in the T4D ecosystem. Applicants should be project decision-makers who can build a roadmap involving documentation, feature specifications, task breakdown, and final execution of implementation.

### Additional Eligibility Criteria

* Target product(s) must have multiple stakeholders: contributors to--and users of—the product represent and/or are funded by multiple organizations.

[Back to Strategic Grant Program Overview](stratagrant.html)
