---
layout: page
title: "OSC Strategic Grant Program - Multi-Stakeholder Collaboration"
---

## Annex 2 - Multi-Stakeholder Collaboration

### Objective

DIAL has identified a critical challenge for free & open source software (FOSS) projects serving the sector: open source products for development are often funded and developed within one organizational host which often lacks the time and resources necessary to foster a more diverse community of contributors and users, leading to fragmentation and siloes in the ecosystems and products that are “open source in name only:” the product’s code is available online and licensed as open-source but remains largely maintained & contributed to by a single organizational host.

### Scope of Work

This grant aims to provide a path for fostering a more diverse and collaborative contributor community for an open source project deemed a global good in the development sector. Applicants should be product champions within the organizational host that can provide a roadmap for documentation, technology transfer & training, code clean-up, governance and coalition building, as well as a list of potential partners who could receive funds to train and ultimately become contributing members to the open source product.

[Back to Strategic Grant Program Overview](stratagrant.html)
